﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fifteen
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<int> values = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0 };
        List<string> winList = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", " " };
        List<Button> buttons = new List<Button>();
        int counter = 0;
        int moves = 0;
        Button noneButton;
        bool game = false;
        Random rnd = new Random();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonInit(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btn.Content = values[counter];
            if (btn.Content.ToString().CompareTo("0") == 0)
            {
                btn.Content = " ";
                noneButton = btn;
            }
            buttons.Add(btn);
            counter++;
            if (buttons.Count == 16 && noneButton != null)
            {
                for (int i = 0; i <= 1000; i++)
                {
                    int rand = rnd.Next(buttons.Count);
                    ButtonClick(buttons[rand], new RoutedEventArgs());
                }
                game = true;
            }
        }
        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            string moveTo = string.Empty;
            bool step = false;
            Button btn = sender as Button;
            if ((int)noneButton.GetValue(Grid.RowProperty) == (int)btn.GetValue(Grid.RowProperty))
            {
                if ((int)noneButton.GetValue(Grid.ColumnProperty) == (int)btn.GetValue(Grid.ColumnProperty) - 1)
                {
                    moveTo = "left";
                    step = true;
                }
                else if((int)noneButton.GetValue(Grid.ColumnProperty) == (int)btn.GetValue(Grid.ColumnProperty) + 1)
                {
                    moveTo = "right";
                    step = true;
                }
            }
            else if((int)noneButton.GetValue(Grid.ColumnProperty) == (int)btn.GetValue(Grid.ColumnProperty))
            {
                if ((int)noneButton.GetValue(Grid.RowProperty) == (int)btn.GetValue(Grid.RowProperty) - 1)
                {
                    moveTo = "up";
                    step = true;
                }
                else if ((int)noneButton.GetValue(Grid.RowProperty) == (int)btn.GetValue(Grid.RowProperty) + 1)
                {
                    moveTo = "down";
                    step = true;
                }
            }
            if (step)
            {
                Animation(btn, moveTo);
            }
        }

        private void Test()
        {
            if (game)
            {
                bool ifWin = true;
                for (int i = 0; i < buttons.Count; i++)
                {
                    if ((buttons[i].Content.ToString()).CompareTo(winList[i])!=0)
                    {
                        ifWin = false;
                        break;
                    }
                }
                moves++;
                if (ifWin)
                {
                    var result = MessageBox.Show($"Moves: {moves}. New game?", "You win!!!", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
                    if (result == System.Windows.MessageBoxResult.No)
                    {
                        this.Close();
                    }
                    else
                    {
                        game = false;
                        noneButton = null;
                        counter = 0;
                        List<Button> helpButtons = buttons;
                        buttons.Clear();
                        moves = 0;
                        for (int i = 0; i< helpButtons.Count; i++)
                        {
                            ButtonInit(helpButtons[i], new EventArgs());
                        }
                         System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                         Application.Current.Shutdown();
                    }
                }
            }
        }

        private void Move(Button btn, Button noneButton)
        {
            int column = (int)btn.GetValue(Grid.ColumnProperty);
            int row = (int)btn.GetValue(Grid.RowProperty);
            btn.SetValue(Grid.ColumnProperty, noneButton.GetValue(Grid.ColumnProperty) as object);
            btn.SetValue(Grid.RowProperty, noneButton.GetValue(Grid.RowProperty) as object);
            noneButton.SetValue(Grid.ColumnProperty, column as object);
            noneButton.SetValue(Grid.RowProperty, row as object);
            int indexNum = buttons.FindIndex((Button b) => { return b == btn; });
            int indexNon = buttons.FindIndex((Button b) => { return b == noneButton; });
            Button helpBtn = buttons[indexNum];
            buttons[indexNum] = buttons[indexNon];
            buttons[indexNon] = helpBtn;
            Test();
        }
        private void Animation(Button btn, string moveTo)
        {
            if (game)
            {
                ThicknessAnimation animation = new ThicknessAnimation();
                double btnWidth = btn.ActualWidth;
                double btnHeight = btn.ActualHeight;
                switch (moveTo)
                {
                    case "left":
                            
                            animation.From = new Thickness(btnWidth+6, 3, 3, 3);
                            animation.To = new Thickness(3, 3, btnWidth + 6, 3);
                            animation.FillBehavior = FillBehavior.Stop;
                            animation.Duration = new Duration(TimeSpan.FromMilliseconds(100)); animation.Completed += (s, e) => {
                                btn.SetValue(Grid.ColumnSpanProperty, 1);
                                btn.SetValue(Grid.ColumnProperty, (int)(btn.GetValue(Grid.ColumnProperty)) + 1);
                                Move(btn, noneButton);
                            };
                        btn.SetValue(Grid.ColumnProperty, (int)(btn.GetValue(Grid.ColumnProperty)) - 1);
                        btn.SetValue(Grid.ColumnSpanProperty, 2);
                            btn.BeginAnimation(Button.MarginProperty, animation);
                        break;
                    case "right":
                        animation.From = new Thickness(3, 3, btnWidth + 6, 3);
                        animation.To = new Thickness(btnWidth + 6, 3, 3, 3);
                        animation.FillBehavior = FillBehavior.Stop;
                        animation.Duration = new Duration(TimeSpan.FromMilliseconds(100)); animation.Completed += (s, e) => {
                            btn.SetValue(Grid.ColumnSpanProperty, 1);
                            Move(btn, noneButton);
                        };
                        btn.SetValue(Grid.ColumnSpanProperty, 2);
                        btn.BeginAnimation(Button.MarginProperty, animation);
                        break;

                    case "down":
                        animation.From = new Thickness(3, 3, 3, btnHeight + 6);
                        animation.To = new Thickness(3, btnHeight + 6, 3, 3);
                        animation.FillBehavior = FillBehavior.Stop;
                        animation.Duration = new Duration(TimeSpan.FromMilliseconds(100)); animation.Completed += (s, e) =>
                        {
                            btn.SetValue(Grid.RowSpanProperty, 1);
                            Move(btn, noneButton);
                        };
                        btn.SetValue(Grid.RowSpanProperty, 2);
                        btn.BeginAnimation(Button.MarginProperty, animation);
                        break;
                    case "up":
                        animation.From = new Thickness(3, btnHeight + 6, 3, 3);
                        animation.To = new Thickness(3, 3, 3, btnHeight + 6);
                        animation.FillBehavior = FillBehavior.Stop;
                        animation.Duration = new Duration(TimeSpan.FromMilliseconds(100)); animation.Completed += (s, e) => {
                            btn.SetValue(Grid.RowSpanProperty, 1);
                            btn.SetValue(Grid.RowProperty, (int)(btn.GetValue(Grid.RowProperty)) + 1);
                            Move(btn, noneButton);
                        };
                        btn.SetValue(Grid.RowProperty, (int)(btn.GetValue(Grid.RowProperty)) - 1);
                        btn.SetValue(Grid.RowSpanProperty, 2);
                        btn.BeginAnimation(Button.MarginProperty, animation);
                        break;
                }
            }
            else
            {
                Move(btn, noneButton);
            }
        }
    }
}
